def calculate_grade(grade):
    if 90 <= grade <= 100:
        return f"{prefix} 5"
    elif 80 <= grade <= 89:
        return f"{prefix} 4"
    elif 70 <= grade <= 79:
        return f"{prefix} 3"
    elif 60 <= grade <= 69:
        return f"{prefix} 2"
    elif 0 <= grade <= 59:
        return f"{prefix} 1"
    else:
        return "Ошибка: Ваше количество баллов вышло за диапазон оценивания."


def validate(grade):
    if grade.isdigit():
        return calculate_grade(int(grade))
    else:
        return "Ошибка: В введённых вами баллах помимо цифр были обнаружены другие символы."


s = input("Введите количество баллов (целое число от 0 до 100): ").strip()
prefix = "Ваша оценка:"

grade = validate(s)
print(grade)
